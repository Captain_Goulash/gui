﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Schieberätsel
{
    public class Tile
    {
        public int id;
        public BitmapImage img;
        public ImageSource src;
        public int colPos;
        public int rowPos;
        public Tile(BitmapImage image, int x, int y)
        {
            img = image;
            colPos = x;
            rowPos = y;
        }
    }
    public class DaPuzzle
    {
        Window win;
        public Tile[,] map = new Tile[2,2];
        public int currcol=0;
        public int currrow=0;
        BitmapImage Picture;
        BitmapImage blankpic;

        public DaPuzzle(Window w,BitmapImage img)
        {
            win = w;
            Picture = new BitmapImage();
            Picture = img;
            currcol = 0;
            currrow = 0;

            blankpic = new BitmapImage();
            blankpic.BeginInit();
            blankpic.UriSource = new Uri(@"F:/Master/GUI/Übungen/Schieberaetsel/Schieberaetsel/AV1.JPG", UriKind.RelativeOrAbsolute);
            blankpic.EndInit();

            map[0, 0] = new Tile((BitmapImage)win.FindResource("blank"), 0, 0);
            map[0, 0].id = 00;
            map[0, 1] = new Tile((BitmapImage)win.FindResource("pic0"), 0, 1);
            map[0, 1].id = 01;
            map[1, 0] = new Tile((BitmapImage)win.FindResource("pic2"), 1, 0);
            map[1, 0].id = 10;
            map[1, 1] = new Tile((BitmapImage)win.FindResource("pic3"), 1, 1);
            map[1,1].id = 11;

            map[0, 0].src = blankpic;
            map[0, 0].id = 00;
            map[1, 0].src = new CroppedBitmap(Picture, new Int32Rect(0 * 200, 1 * 200, 200, 200));
            map[1, 0].id = 10;
            map[0, 1].src = new CroppedBitmap(Picture, new Int32Rect(1 * 200, 0 * 200, 200, 200));
            map[0, 1].id = 01;
            map[1, 1].src = new CroppedBitmap(Picture, new Int32Rect(1 * 200, 1 * 200, 200, 200));
            map[1, 1].id = 11;
        }

        public void initSolution()
        {
            map[0, 0].src = new CroppedBitmap(Picture, new Int32Rect(0 * 200, 0 * 200, 200, 200));
            map[0, 0].id = 00;
            map[1, 0].src = new CroppedBitmap(Picture, new Int32Rect(1 * 200, 0 * 200, 200, 200));
            map[1, 0].id = 10;
            map[0, 1].src = new CroppedBitmap(Picture, new Int32Rect(0 * 200, 1 * 200, 200, 200));
            map[0,1].id = 01;
            map[1, 1].src = new CroppedBitmap(Picture, new Int32Rect(1 * 200, 1 * 200, 200, 200));
            map[1,1].id = 11;
        }

        public Tile getTile(int x, int y)
        {
            return map[x, y];
        }

        public Tile getCurrentTile()
        {
            return getTile(currcol, currrow);
        }

        public void swaptile(Tile currT, Tile destT)
        {
            ImageSource tmp = destT.src;
            destT.src = currT.src;
            currT.src = tmp;
        }

        public void swaptile(int nextX, int nextY)
        {
            Tile tmp=map[currcol,currrow];
            map[currcol, currrow] = map[nextX, nextY];
            map[nextX, nextY] = tmp;
        }
    }

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage theSolution;
        int nextrow = 0;
        int nextcol = 0;
        DaPuzzle p;
        DaPuzzle sol;
       
        public bool checkforWin()
        {
            for(int i=0;i<sol.map.GetLength(0);i++)
            {
                for (int j = 0; j < sol.map.GetLength(1); j++)
                {
                    if (sol.map[i, j].id != p.map[i, j].id)
                    {
                        return false;
                    }
                }
             }
            return true;
        }


        public MainWindow()
        {
            InitializeComponent();

            theSolution = new BitmapImage();
            theSolution.BeginInit();
            theSolution.UriSource = new Uri(@"F:/Master/GUI/Übungen/Schieberaetsel/Schieberaetsel/Gestiefelter_Kater400.bmp", UriKind.RelativeOrAbsolute);
            theSolution.EndInit();
            
            p=new DaPuzzle(this,theSolution);
            sol = new DaPuzzle(this, theSolution);
            sol.initSolution();

            Console.WriteLine("Width : " + theSolution.Width + "height : " + theSolution.Height);
            solution0.Source = sol.getTile(0,0).src;
            solution1.Source = sol.getTile(1,0).src;
            solution2.Source = sol.getTile(0,1).src;
            solution3.Source = sol.getTile(1,1).src;

            apply();
        }
        public void apply()
        {
            puzzle0.Source = p.getTile(0, 0).src;
            puzzle1.Source = p.getTile(0, 1).src;
            puzzle2.Source = p.getTile(1, 0).src;
            puzzle3.Source = p.getTile(1, 1).src;
            if (checkforWin() == true)
            {
                Console.WriteLine("GEWONNEN");
            }
        }

        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                nextrow = nextrow - 1;
                if (nextrow < 0) nextrow = 0;
                Console.WriteLine("LEFT! col:"+nextcol+" row:"+nextrow);
            }

            if (e.Key == Key.Up)
            {
                nextcol = nextcol - 1;
                if (nextcol < 0) nextcol = 0;
                Console.WriteLine("UP! col:" + nextcol + " row:" + nextrow);
            }

            if (e.Key == Key.Down)
            {
                nextcol = nextcol + 1;
                if (nextcol >1) nextcol = 1;
                Console.WriteLine("DOWN! col:" + nextcol + " row:" + nextrow);
            }

            if (e.Key == Key.Right)
            {
                nextrow = nextrow + 1;
                if (nextrow >1) nextrow = 1;
                Console.WriteLine("RIGHT! col:" + nextcol + " row:" + nextrow);
            }

            p.swaptile(nextcol, nextrow);

            p.currcol = nextcol;
            p.currrow = nextrow;
            apply();
        }
    }


}
