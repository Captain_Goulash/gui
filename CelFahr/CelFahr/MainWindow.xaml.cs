﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace CelFahr
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {
        private double celstofahr(double a)
        {
            return a * 1.8 +32;
        }

        private double fahrtocels(double a)
        {
            return ((a - 32) * 5 / 9);
        }

        public MainWindow()
        {
            InitializeComponent();
            inputtext.Text = "0";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a = 0;
            char convertto = inputtext.Text[inputtext.Text.Length - 1];
            string input = inputtext.Text.Remove(inputtext.Text.Length - 1);
           
            Console.WriteLine(convertto);
            Console.WriteLine(input);         
            if (IsTextAllowed(input))
            {
                    a = double.Parse(input);

                    if(convertto=='F'||convertto=='f')
                    {
                        result_celsius.Content = fahrtocels(a) + "C";
                        result_fahrenheit.Content = a+"F";
                    }
                    if(convertto=='C'||convertto=='f')
                    {
                        result_fahrenheit.Content = celstofahr(a) + "F";
                        result_celsius.Content = a+"C";
                    }
            }    
        }
        private bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+");
            return !regex.IsMatch(text);
        }


    }
}
